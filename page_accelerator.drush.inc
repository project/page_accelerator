<?php

/**
 * @file
 * Drush integration for pageAccelerator.
 */

// URL to download the pageAccelerator JavaScript file.
define('PAGE_ACCELERATOR_URL', 'https://raw.githubusercontent.com/Easyfood/pageAccelerator/master/dist/page-accelerator.min.js');

/**
 * Implements hook_drush_command().
 */
function page_accelerator_drush_command() {

  $items['page-accelerator-download'] = array(
    'description' => dt('Downloads the required page-accelerator.min.js file.'),
    'options' => array(
      'force' => dt('Downloads the library even if it already exists.'),
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function page_accelerator_drush_help($section) {

  switch ($section) {
    case 'drush:page-accelerator-download':
      return dt("Downloads the required JavaScript file for pageAccelerator. Use with '--force' to overwrite existing file.");
  }
}

/**
 * Downloads page_accelerator.js file.
 */
function drush_page_accelerator_download() {

  if (!function_exists('libraries_get_path')) {
    drush_log(dt('Libraries module is required.'), 'error');
    return FALSE;
  }

  $path = libraries_get_path('page_accelerator');
  if (!$path) {
    $path = 'sites/all/libraries/page_accelerator';
  }

  // Create the path if it does not exist yet.
  if (!is_dir($path)) {
    drush_mkdir($path);
  }

  // Download the file.
  if (is_file($path . '/page-accelerator.min.js') && !drush_get_option('force')) {
    drush_log(dt('page-accelerator.min.js file already exists. No download required. Use --force to overwrite.'), 'ok');
    return TRUE;
  }
  elseif (drush_op('chdir', $path) && (drush_shell_exec('curl -O ' . PAGE_ACCELERATOR_URL) || drush_shell_exec('wget --no-check-certificate ' . PAGE_ACCELERATOR_URL))) {
    drush_log(dt('Latest page-accelerator.min.js file has been downloaded to @path', array('@path' => $path)), 'success');
    return TRUE;
  }
  else {
    drush_log(dt('Drush was unable to download page-accelerator.min.js file to @path', array('@path' => $path)), 'error');
    return FALSE;
  }
}

/**
 * Implements drush_MODULE_post_COMMAND().
 *
 * When enabling the module Drush will try to download page-accelerator.min.js file.
 */
function drush_page_accelerator_post_pm_enable() {

  $extensions = func_get_args();

  // Deal with comma delimited extension list.
  if (strpos($extensions[0], ',') !== FALSE) {
    $extensions = explode(',', $extensions[0]);
  }

  if (in_array('page_accelerator', $extensions) && !drush_get_option('skip')) {

    // Download the library.
    drush_page_accelerator_download();
  }
}
