pageAccelerator is very light solution to load web pages faster.


Credits
-------
See https://github.com/Easyfood/pageAccelerator

This Drupal module is maintained by Pere Orga <pere@orga.cat>.
